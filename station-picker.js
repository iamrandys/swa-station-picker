'use strict';

angular.module('swaStationPicker', ['swaStations', 'swaRoutes'])
    .directive('stationPicker', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                id: '@',
                title: '@',
                stationCode: '=ngModel',
                to: '=',
                from: '=',
                onStationSelected: '&',
                filter: '@'
            },
            controller: 'stationsCtrl',
            templateUrl: 'templates/station-picker.html'
        };
    })

    .controller('stationPairCtrl', function ($scope, routesService) {
        $scope.clearToIfNotValid = function () {
            if ($scope.search.form.to && $scope.search.form.from && !routesService.isValidToStation($scope.search.form.from, $scope.search.form.to)) {
                $scope.search.form.to = 'AAA';
            }
        };

        $scope.$watch('search.form.from', $scope.clearToIfNotValid);

    })

    .controller('stationsCtrl', function ($scope, stationsService) {
        $scope.stations = stationsService.stations;
        $scope.isOpen = false;

        $scope.open = function () {
            $scope.isOpen = true;
        };

        $scope.close = function () {
            $scope.isOpen = false;
        };

        $scope.buildTypeahead = function (from) {
            var filter = 'value.code as value.display_name for value in stations |';
            if (from) {
                filter += ' destinationsFromFilter:from |';
            }
            return filter + ' filter:$viewValue | orderBy:display_name | limitTo:8';
        };
    })
;